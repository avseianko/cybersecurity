use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('CyberSecurity');

# HTML/XML
$t->get_ok('/')->status_is(404); #->text_is('div#message' => 'Hello!');
 
# JSON
$t->post_ok('/register/' => {Accept => '*/*'} => json => {email => 'email10@email.com', password => 'password', name => 'Иван Иванов'})
  ->status_is(200)
  ->json_has('/token');

done_testing();
