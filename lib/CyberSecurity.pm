package CyberSecurity;

use strict;
use utf8;
use warnings;

use Mojo::Base 'Mojolicious';

use Mojo::mysql;
use Mojo::JSON qw(decode_json encode_json);
use Digest::SHA;
use Mojo::Cookie::Response;
use POSIX qw/strftime/;

sub startup {
	my $self = shift;

	my $r = $self->routes;

	$self->helper( mysql =>
		sub { state $mysql = Mojo::mysql->strict_mode('mysql://cyber_security:password@localhost/cyber_security;mysql_ssl=0'); }
	);

	$self->mysql->db->query("SET NAMES utf8");

	$r->any('/register' => sub {
		my $c = shift;
		my $user_request = eval { decode_json($c->req->body); };
		if ( $@ ) {
			$c->render( json => { error => "Wrong JSON" } );
			return;
		}

		my $result = {};
		unless ( $user_request->{email} && $user_request->{password} ){
			$c->render( json => { error => "No email or password in request" } );
			return;
		}
		my $session_id = $self->create_session_id;
		eval {
			my $res = $c->mysql->db->insert('users', {
				name => $user_request->{name},
				email => $user_request->{email},
				password => Digest::SHA::sha256_base64( $user_request->{password} ),
				ctime => strftime "%F %T", localtime $^T,
			});

			my $user_id = $res->last_insert_id;
			$c->mysql->db->query('insert into sessions (user_id, session_id, session_content, ctime) VALUES (?, ?, ?, ?)', $user_id, $session_id, '{}', strftime "%F %T", localtime $^T);
		};
		if ( $@ ) {
			$c->render( json => { error => "Cannot add user: " . $@ } );
			return;
		}
		my $cookie = Mojo::Cookie::Response->new;
		$cookie->name('token');
		$cookie->value($session_id);
		$cookie->expires(time + 60*60*24*7); # 1 week

		$c->res->headers->set_cookie($cookie->to_string);
		$c->render( json => {"token" => $session_id } );
		return;
	});


	$r->any('/login' => sub {
		my $c = shift;
		my $user_request = eval { decode_json($c->req->body); };
		if ( $@ ) {
			$c->render( json => { error => "Wrong JSON" } );
			return;
		}

		my $result = {};
		unless ( $user_request->{email} && $user_request->{password} ){
			$c->render( json => { error => "No email or password in request" } );
			return;
		}
		my $session_id = $self->create_session_id;

		eval {
			my $res = $c->mysql->db->select('users', undef, {
				email => $user_request->{email},
				password => Digest::SHA::sha256_base64( $user_request->{password} ),
			})->hash;
			unless ( $res && $res->{id} ){
				$c->render( json => { error => "No such user" } );
				return;
			}

			my $user_id = $res->{id};
			$c->mysql->db->query('insert into sessions (user_id, session_id, session_content, ctime) VALUES (?, ?, ?, ?)', $user_id, $session_id, '{}', strftime "%F %T", localtime $^T);
		};
		if ( $@ ) {
			$c->render( json => { error => "Cannot login: " . $@ } );
			return;
		}
		my $cookie = Mojo::Cookie::Response->new;
		$cookie->name('token');
		$cookie->value($session_id);
		$cookie->expires(time + 60*60*24*7); # 1 week

		$c->res->headers->set_cookie($cookie->to_string);
		$c->render( json => {"token" => $session_id } );
		return;
	});

	$r->any('/get_info' => sub {
		my $c = shift;
		my $user_id = "";
		unless ( $user_id = $self->authorize( $c ) ) {
			$c->render( json => { error => "Not authorized" } );
			return;
		}

		my $user = $c->mysql->db->select('users', undef, {id => $user_id})->hash;
		my $results = $c->mysql->db->query('select * from user_points where user_id = ?', $user_id);
		my $totalScore = 0;
		while (my $next = $results->hash) {
			$totalScore += $next->{points};
		}
		$c->render( json => {"totalPoints" => $totalScore, email => $user->{email}, name => $user->{name}, ctime => $user->{ctime} } );
	});

	$r->any('/get_categories' => sub {
		my $c = shift;

		my $results = $c->mysql->db->query('select * from categories');
		my $totalScore = 0;
		my $result = {result => []};
		while (my $next = $results->hash) {
			push @{ $result->{result} }, $next;
		}
		$c->render( json => $result );
	});

	$r->any('/get_next_question' => sub {
		my $c = shift;
		my $user_id;
		unless ( $user_id = $self->authorize( $c ) ) {
			$c->render( json => { error => "Not authorized" } );
			return;
		}

		my $user_request = eval { decode_json($c->req->body); };
		if ( $@ ) {
			$c->render( json => { error => "Wrong JSON" } );
			return;
		}

		if ( !$user_request->{category} ) {
			$c->render( json => { error => "No parameter 'category' in JSON" } );
			return;
		}

		my $user = $c->mysql->db->select('users', undef, {id => $user_id})->hash;
		my $results = $c->mysql->db->query('select * from user_points where user_id = ? and category_id = ?', $user_id, $user_request->{category})->hash;
		my $user_points = 0;
		$user_points = $results->{ points } if $results && $results->{points};

		$results = $c->mysql->db->query('select answer_id from user_answers where user_id = ? ORDER BY ctime ASC, id ASC', $user_id);
		my $answers = {};
		my $last_answer = 0;
		while (my $next = $results->hash) {
			$answers->{ $next->{answer_id} } = ($answers->{ $next->{answer_id} } || 0) + 1;
			$last_answer = $next->{answer_id};
		}

		my $questions = {};
		my $last_question = 0;
		if (keys %$answers) {
			my @all_answers = keys %$answers;
			my $q_res = $c->mysql->db->query(
				'select id, question_id from answers where id IN (' . ( join ',', (('?')x(scalar @all_answers + 1)) ) . ')', (0, @all_answers)
			);
			while (my $next = $q_res->hash) {
				$questions->{ $next->{question_id} } = $next->{id};
				$last_question = $next->{question_id} if $last_answer == $next->{id};
			}
		}

		# Trying first to get question which was not answered by user
		my @all_questions = keys %$questions;
		my $question = $c->mysql->db->query(
			'select * from questions where min_points_limit <= ? and max_points_limit >= ? and category_id = ? and id NOT IN (' . join(',', (('?')x(scalar @all_questions + 1))) . ')',
			$user_points < 0 ? 0 : $user_points, $user_points, $user_request->{category}, (@all_questions, 0)
		)->hash;

		unless ($question && $question->{id}) {
			$question = $c->mysql->db->query(
				'select * from questions where min_points_limit <= ? and category_id = ? and id <> ? order by rand()',
				$user_points < 0 ? 0 : $user_points, $user_request->{category}, $last_question
			)->hash;
		}

		my $return_data = {
			id => $question->{id},
			image => $question->{image},
			text => $question->{content},
			category => $question->{category_id},
			min_points_limit => $question->{min_points_limit},
			max_points_limit => $question->{max_points_limit},
			answers => [],
		};

		my $q_answers = $c->mysql->db->select(
			'answers', undef, { question_id => $question->{id} }
		);
		while (my $ans = $q_answers->hash ) {
			push @{ $return_data->{answers} }, {
				id => $ans->{id},
				text => $ans->{content},
				sort_order => $ans->{sort_order},
			};
		}

		$c->render( json => $return_data );
	});

	$r->any('/set_answer' => sub {
		my $c = shift;

		my $user_id = "";
		unless ( $user_id = $self->authorize( $c ) ) {
			$c->render( json => { error => "Not authorized" } );
			return;
		}

		my $user_request = eval { decode_json($c->req->body); };
		if ( $@ ) {
			$c->render( json => { error => "Wrong JSON" } );
			return;
		}

		if ( !$user_request->{answer} ) {
			$c->render( json => { error => "No parameter 'answer' in JSON" } );
			return;
		}

		my $answer = $c->mysql->db->select('answers', undef, {id => $user_request->{answer}})->hash;
		unless ( $answer && $answer->{id} ) {
			$c->render( json => { error => "No such answer" } );
			return;
		}

		my $question = $c->mysql->db->select('questions', undef, {id => $answer->{question_id}})->hash;
		unless ( $question && $question->{category_id} ) {
			$c->render( json => { error => "No such question" } );
			return;
		}

		my $res = $c->mysql->db->insert('user_answers', {
			user_id => $user_id,
			answer_id => $user_request->{answer},
			ctime => strftime "%F %T", localtime $^T,
		});

		eval {
			$c->mysql->db->insert('user_points', {
				points => $answer->{points},
				user_id => $user_id,
				category_id => $question->{category_id}
			});
		};
		if ( $@ ) {
			$c->mysql->db->query('UPDATE user_points SET points = points + ? WHERE user_id = ? AND category_id = ?',
				$answer->{points}, $user_id, $question->{category_id}
			);
		}
		$c->render( json => {
			points => $answer->{ points },
		});
	});
}

sub authorize {
	my $self = shift;
	my $c = shift;

	my $token = $c->cookie('token');
	return undef unless $token;
	my $user = eval { $c->mysql->db->select('sessions', undef, {session_id => $token})->hash->{user_id}; };
	return $user;
}

sub create_session_id {
	my $session_id = "";
	for (0..254) { $session_id .= chr( int(rand(25) + 65) ); }
	return $session_id;
}

1;
