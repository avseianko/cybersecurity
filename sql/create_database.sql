SET NAMES 'utf8';

DROP DATABASE cyber_security;
CREATE DATABASE IF NOT EXISTS cyber_security;
USE cyber_security;
CREATE TABLE users (
	id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) DEFAULT NULL,
	password VARCHAR(255) DEFAULT NULL,
	email VARCHAR(255) DEFAULT NULL,
	ctime DATETIME DEFAULT 0,
	UNIQUE KEY(email)
) Engine=InnoDB;

CREATE TABLE IF NOT EXISTS sessions (
	user_id BIGINT UNSIGNED NOT NULL,
	session_id VARCHAR(255) NOT NULL PRIMARY KEY,
	session_content TEXT NOT NULL DEFAULT "",
	ctime DATETIME NOT NULL DEFAULT 0,
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) Engine=InnoDB;

CREATE TABLE IF NOT EXISTS categories (
	id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL DEFAULT "",
	image TEXT,
	UNIQUE KEY (name)
) Engine=InnoDB;

INSERT INTO categories (id, name, image) VALUES (1, 'Аутентификация', 'http://www.google.com');
INSERT INTO categories (id, name, image) VALUES (2, 'Безопасность данных', 'http://www.google.com');
INSERT INTO categories (id, name, image) VALUES (3, 'Социальная инженерия', 'http://www.google.com');


CREATE TABLE IF NOT EXISTS user_points (
	user_id BIGINT UNSIGNED NOT NULL REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
	category_id BIGINT UNSIGNED NOT NULL REFERENCES categories(id) ON DELETE CASCADE ON UPDATE CASCADE,
	points BIGINT DEFAULT 0,
	PRIMARY KEY (user_id, category_id)
) Engine=InnoDB;

CREATE TABLE question_sets (
	id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255)
) Engine=InnoDB;

INSERT INTO question_sets VALUES (1, 'test');

CREATE TABLE IF NOT EXISTS questions (
	id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	set_id BIGINT UNSIGNED NOT NULL REFERENCES question_sets(id),
	image TEXT DEFAULT NULL,
	content TEXT NOT NULL DEFAULT "",
	category_id BIGINT UNSIGNED NOT NULL REFERENCES categories(id),
	min_points_limit BIGINT NOT NULL DEFAULT 0,
	max_points_limit BIGINT NOT NULL DEFAULT 999999,
	time_limit TIME NOT NULL DEFAULT '00:00:10'
) Engine=InnoDB;

INSERT INTO questions VALUES(1, 1, 'http://www.yandex.ru/', 'Сколько будет 2x2?', 1, 0, 10, '00:00:30');
INSERT INTO questions VALUES(2, 1, 'http://www.yandex.ru/', 'Жи ши пиши с буковой Ы?', 2, 0, 10, '00:00:30');
INSERT INTO questions VALUES(3, 1, 'http://www.yandex.ru/', 'Сколько будет 3x3?', 1, 0, 10, '00:00:30');
INSERT INTO questions VALUES(4, 1, 'http://www.yandex.ru/', 'Сколько будет 90x90?', 1, 11, 20, '00:00:30');


CREATE TABLE IF NOT EXISTS answers (
	id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	question_id BIGINT UNSIGNED NOT NULL REFERENCES questions(id) ON DELETE CASCADE ON UPDATE CASCADE,
	content TEXT NOT NULL DEFAULT "",
	points BIGINT NOT NULL DEFAULT 0,
	sort_order BIGINT NOT NULL DEFAULT 0
) Engine=InnoDB;

INSERT INTO answers VALUES (1, 1, '4', 1, 1);
INSERT INTO answers VALUES (2, 1, '5', 0, 2);

INSERT INTO answers VALUES (3, 2, 'Да', 1, 1);
INSERT INTO answers VALUES (4, 2, 'Нет', 0, 2);

INSERT INTO answers VALUES (5, 3, '9', 1, 1);
INSERT INTO answers VALUES (6, 3, '8', 0, 2);

INSERT INTO answers VALUES (7, 4, '1800', 10, 1);
INSERT INTO answers VALUES (8, 4, '1111', -5, 2);



CREATE TABLE IF NOT EXISTS user_answers (
	id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	user_id BIGINT UNSIGNED NOT NULL REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
	answer_id BIGINT UNSIGNED NOT NULL REFERENCES answers(id) ON DELETE CASCADE ON UPDATE CASCADE,
	ctime DATETIME NOT NULL DEFAULT 0
) Engine=InnoDB;

